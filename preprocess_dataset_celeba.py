import os

from mtcnn import MTCNN

from utils import extract_faces_and_save_flat

BASE_PATH = os.path.expanduser('~/Datasets/img_align_celeba')
DATASET_PATH = os.path.join(BASE_PATH, 'img')
NEW_DATASET_PATH = os.path.join(BASE_PATH, 'faces')

os.makedirs(NEW_DATASET_PATH, exist_ok=True)

face_detector = MTCNN()
extract_faces_and_save_flat(face_detector, DATASET_PATH, NEW_DATASET_PATH)

