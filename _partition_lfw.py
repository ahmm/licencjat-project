import os
import shutil

INPUT_PATH = os.path.expanduser('~/Datasets/lfw_faces_96')
OUTPUT_PATH = os.path.expanduser('~/Datasets/lfw_faces_96_10')

N = 10

for class_entry in os.scandir(INPUT_PATH):

    if len(os.listdir(class_entry.path)) < N:
        continue

    new_class_path = os.path.join(OUTPUT_PATH, class_entry.name)
    print(f'Copying dir {class_entry.path} to {new_class_path}')

    shutil.copytree(class_entry.path, new_class_path)

# {1: (5749, 13233), 2: (1680, 9164), 3: (901, 7606), 4: (610, 6733), 5: (423, 5985),
# 6: (311, 5425), 7: (256, 5095), 8: (217, 4822), 9: (184, 4558), 10: (158, 4324)}

