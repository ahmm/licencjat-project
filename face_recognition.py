import argparse
import logging
import math
import os
import pickle
from typing import List

from matplotlib.patches import Rectangle
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from tensorflow.keras import Model
from tensorflow.keras.models import load_model

from utils import FaceRecognitionException, extract_faces_from_image

import matplotlib
matplotlib.use('GTK3Agg')

logging.getLogger("PIL.TiffImagePlugin").setLevel(logging.WARNING)
import matplotlib.pyplot as plt
from mtcnn import MTCNN
import numpy as np

from tensorflow.keras.applications.resnet_v2 import preprocess_input


def get_parser():
    parser = argparse.ArgumentParser(description="Face detection")

    parser.add_argument("--classifier_path", type=str)
    parser.add_argument("--dir", type=str, required=True)
    parser.add_argument("--model_path", type=str, required=True)

    parser.add_argument(
        "--action",
        required=True,
        choices=("train", "predict"),
    )

    # action-specific options:
    parser.add_argument(
        "--classification_method", type=str, choices=("svm", "knn"), default="knn"
    )
    parser.add_argument("--highlight_faces", action="store_true")
    return parser


def main(
        classifier_path: str,
        images_dir: str,
        action: str,
        should_highlight_faces: bool,
        model_path: str,
        classification_method: str,
):
    face_detector = MTCNN()

    model = get_model(model_path)

    if action == 'train':
        logging.info("Training classifier...")
        train(images_dir, model, classification_method, model_save_path=classifier_path)
        logging.info("Training complete")
    elif action == 'predict':
        predict(face_detector, model, classifier_path, images_dir, should_highlight_faces)
    elif action == 'validate':
        pass


def predict(
        face_detector: MTCNN,
        model,
        classifier_path: str,
        test_dir: str,
        should_highlight_faces: bool,
):

    try:
        with open(classifier_path, "rb") as f:
            classifier = pickle.load(f)
    except FileNotFoundError:
        logging.error(f"{classifier_path} does not exist. Use valid path")
        exit(1)
    except Exception:
        logging.error(f"{classifier_path} is not a valid classifier")
        exit(2)

    for image_entry in os.scandir(test_dir):
        logging.info(f'Processing {image_entry}')
        try:
            face_pixels, face_bounding_boxes = extract_faces_from_image(
                face_detector, image_entry.path, required_size=(96, 96)
            )
        except Exception as e:
            logging.error(f"Unable to extract faces from image {image_entry.name}")
            logging.error(e)
            continue

        samples = []
        for pixels in face_pixels:
            samples.append(np.asarray(pixels, 'float32'))
        samples = preprocess_input(np.array(samples))
        face_embeddings = model.predict(samples)

        predictions = classifier.predict(face_embeddings)
        all_probabilities = classifier.predict_proba(face_embeddings)
        best_class_indices = np.argmax(all_probabilities, axis=1)
        probabilities = all_probabilities[
            np.arange(len(best_class_indices)), best_class_indices
        ]

        if should_highlight_faces:
            highlight_faces(
                image_entry.path, face_bounding_boxes, predictions, probabilities
            )
        else:
            print(image_entry.name, predictions, probabilities)


def highlight_faces(
        image_path: str,
        bounding_boxes: List[dict],
        predictions: List[str],
        probabilities: List[float],
) -> None:
    """
    Display an image with a red box drawn around a face and a caption with identity
    and probability.

    :param image_path:
    :param bounding_boxes:
    :param predictions:
    :param probabilities:
    """
    image = plt.imread(image_path)
    plt.imshow(image)

    ax = plt.gca()

    for bounding_box, prediction, probability in zip(
            bounding_boxes, predictions, probabilities
    ):
        x, y, width, height = bounding_box

        face_border = Rectangle((x, y), width, height, fill=False, color="#cf3721")
        ax.add_patch(face_border)

        # Round to two significant digits so that we can display them nicely
        # no matter if the probability is 1.0 or 0.00025
        round_significant = lambda n: round(n, int(abs(math.log10(n))) + 2)

        highlight_text = f"{prediction}\n{round_significant(probability)}"

        ax.text(
            x + width / 2,
            y + height + 20,
            highlight_text,
            fontsize=11,
            color="#cf3721",
            horizontalalignment="center",
            verticalalignment="top",
            bbox=dict(facecolor="black", alpha=0.5, boxstyle="square, pad=0.1"),
            )

    plt.show()


def get_model(model_path):
    model = load_model(model_path)

    base_model = Model(
        inputs=model.input,
        outputs=model.get_layer(name='global_avg_pool').output
    )

    return base_model


def train(
        train_dir: str,
        model,
        classification_method: str,
        model_save_path: str = None,
        **classifier_config,
):
    X, y = get_face_embeddings(train_dir, model)

    # classifier part:
    if classification_method == "knn":
        n_neighbors = classifier_config.get("n_neighbors", 3)
        classifier = KNeighborsClassifier(
            n_neighbors=n_neighbors, algorithm="ball_tree", weights="distance"
        )
    elif classification_method == "svm":
        classifier = svm.SVC(kernel="linear", probability=True)
    else:
        raise FaceRecognitionException(
            "Unknown classification_method, valid options are 'kvm' or 'svm'"
        )

    logging.info("Training the classifier...")
    classifier.fit(X, y)
    logging.info("Finished training the classifier")

    if model_save_path:
        with open(model_save_path, "wb") as f:
            pickle.dump(classifier, f)

    return classifier


def get_face_embeddings(faces_dir, model):
    """
    Use `model` to get face embeddings for each face image in `faces_dir`. The expected
    structure of the directory is as follows:

    faces_dir
    |- Person1
      |- 0001.jpg
      |- 0002.jpg
    |- Person2
      |- 0001.jpg

    The Person1, Person2... directory names are taken as labels for images inside it.

    :param faces_dir: directory containing extracted face images
    :param model: model to use for extracting the embeddings
    :return: tuple of (embeddings, label)
    """

    X = []
    y = []

    for class_entry in os.scandir(faces_dir):
        if not class_entry.is_dir():
            continue

        logging.info(f"Processing {class_entry.name}")

        for image_entry in os.scandir(class_entry.path):
            face = plt.imread(image_entry.path)
            samples = preprocess_input(np.asarray([face], "float32"))
            face_embedding = model.predict(samples)[0]

            X.append(face_embedding)
            y.append(class_entry.name)

    return X, y


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format="%(filename)s: %(message)s")

    args = get_parser().parse_args()

    main(
        classifier_path=args.classifier_path,
        images_dir=args.dir,
        action=args.action,
        should_highlight_faces=args.highlight_faces,
        model_path=args.model_path,
        classification_method=args.classification_method,
    )
