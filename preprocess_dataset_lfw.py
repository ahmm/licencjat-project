import os

from mtcnn import MTCNN

from utils import extract_faces_and_save

DATASET_PATH = os.path.expanduser('~/Datasets/lfw')
NEW_DATASET_PATH = os.path.expanduser('~/Datasets/lfw_faces_96')

face_detector = MTCNN()
extract_faces_and_save(face_detector, DATASET_PATH, NEW_DATASET_PATH, required_size=(96, 96))

