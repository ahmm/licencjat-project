import os
from typing import List, Tuple

import numpy as np
from PIL import Image
from matplotlib import pyplot as plt
from mtcnn import MTCNN
from tensorflow.keras import backend as K


def get_names_from_filename(filename: str) -> List[str]:
    """
    The following convention for naming image files is expected:
        `name_surname__name2_surname2--2.jpg`

    This function extracts names that can be used for evaluation, e.g.
    >>> get_names_from_filename("Tracy_McGrady__LeBron_James--2")
    ["tracy_mcgrady", "lebron_james"]

    :param filename: name of image file in proper convention
    :return: list of names found in the filename
    """

    try:
        filename = filename.lower()
        filename = filename.split(".")[0]  # remove extension
        filename = filename.split("-")[0]  # remove id number at the end
        names = filename.split("__")  # split multiple names
        return names
    except Exception as e:
        raise FaceRecognitionException(e)


class FaceRecognitionException(Exception):
    """ Base class for all exceptions raised from within the program. """


def euclidean_distance(vectors):
    x, y = vectors
    return K.sqrt(K.sum(K.square(x - y), axis=1, keepdims=True))


def extract_faces_and_save_flat(detector: MTCNN, input_dir: str, output_dir: str):
    for i, image_entry in enumerate(os.scandir(input_dir)):

        try:
            faces, _ = extract_faces_from_image(detector, image_entry.path)

            # if there is more than one face, pick the largest
            if len(faces > 1):
                largest_i = faces.index(max(face.size))
                face = faces[largest_i]
            else:
                face = faces[0]
        except Exception as e:
            print(f'Unable to extract face from {image_entry.path}: {e}')
            continue

        plt.imsave(os.path.join(output_dir, image_entry.path), face)

        if i % 100 == 0:
            print(i)


def extract_faces_and_save(detector: MTCNN, input_dir: str, output_dir: str, required_size=(224, 224)):
    for class_entry in os.scandir(input_dir):
        if not class_entry.is_dir():
            continue

        print(f"Extracting faces for {class_entry.name}")

        new_class_path = os.path.join(output_dir, class_entry.name)
        os.makedirs(new_class_path, exist_ok=True)

        for image_entry in os.scandir(class_entry.path):
            try:
                faces, _ = extract_faces_from_image(detector, image_entry.path)

                # if there is more than one face, pick the largest
                face_image = max(faces, key=lambda x: x.size[0] * x.size[1])

                face_image = face_image.resize(required_size)
                face = np.asarray(face_image)

            except Exception as e:
                print(f"{image_entry.name}: {e}")
                continue

            plt.imsave(os.path.join(new_class_path, image_entry.name), face)


def extract_faces_from_image(detector: MTCNN, image_path: str, required_size=None) -> Tuple[List, List]:

    image = plt.imread(image_path)
    detected_faces = detector.detect_faces(image)

    face_images = []
    face_boxes = []

    for face in detected_faces:
        x, y, width, height = face["box"]

        # MTCNN can return negative coordinates if the detected
        # face is near the edge of the image
        x = max(x, 1)
        y = max(y, 1)

        face_boundary = image[y : y + height, x : x + width]

        face_image = Image.fromarray(face_boundary)

        if required_size:
            face_image = face_image.resize(required_size)

        face_images.append(face_image)
        face_boxes.append(face["box"])

    return face_images, face_boxes